from flask import Flask, render_template,request,session,redirect,url_for
from argon2 import PasswordHasher
import os, sys, datetime, sqlite3, time
from waitress import serve
from validate_email import validate_email


app = Flask(__name__)
app.debug = True
app.secret_key = 'super secret key'
os.environ['PORT'] = '8080'


@app.route('/',methods=['GET','POST'])
def index():
    if session.get('loged_in') == True:
        return render_template('index.html',user = session.get('username'))
    else:
        return render_template('login.html')


@app.route('/logout',methods=['GET','POST'])
def logout():
    session.pop('username',None)
    session.pop('loged_in',None)
    return redirect(url_for('index'))


@app.route('/login',methods=['GET','POST'])
def login():
    with sqlite3.connect('login.db') as connection:
        cursor = connection.cursor()
        username = request.form['username']
        password = request.form['password']

        ph = PasswordHasher()
        cursor.execute('SELECT username FROM users')
        usernames = cursor.fetchall()
        for user in usernames:
            if user[0] == username:
                cursor.execute('SELECT password from users')
                passwords = cursor.fetchall()
                for pwd in passwords:
                    try:
                        if ph.verify(pwd[0],password) == True:
                            session['username'] = username
                            session['loged_in'] = True
                            return redirect(url_for('index'))
                    except:
                        pass
        session['login_error'] = True
        return redirect(url_for('index'))


@app.route('/registration',methods=['GET','POST'])
def registration():

    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        age = request.form['age']
        username = request.form['username']
        password = request.form['password']


        register_error = ''
        if len(password)<6:
            register_error +='*password field must be longer than 6 characters \r'
        if len(username)<6:
            register_error +='*username field must be longer than 6 characters \r'
        try:
            if int(age)<0 and int(age) >120:
                register_error +='*age field can not be negative or greater than 120 \n'
        except ValueError:
                register_error +='*age field can not be negative or greater than 120 \n'
        if len(name)<1:
            register_error +='*name field can not be empty \n'
        if not validate_email(email):
            register_error +='*wrong email format \n'

        if not register_error:
            ph = PasswordHasher()
            hash = ph.hash(password)
            insert_user(name,email,age,username,hash)   
            return render_template('login.html')
        else:
            return render_template('registration.html', register_error=register_error)

    return render_template('registration.html')


@app.route('/confirm_email/<token>')
def confirm_email(token):
    try:
        try:
            email = s.loads(token,salt='my_salt',max_age=3600)  #age in seconds
        except SignatureExpired:
            return "<h1>The confirmation link expired!</h1>"
    except BadTimeSignature:
        return "<h1>It has been used an invalid confirmation link!</h1>"
    return '<h1>Succefull confirmation!!!</h1>'


@app.route('/view_files')
def view_files():
    if session.get('loged_in') == True:

        path = 'user_files'+'/'+session.get('username')  #build the path to user's folder
        if os.path.isdir(path):                           #check is the user's folder exists
            dirs = os.listdir(path)
            files = '''<table class='table table-hover'>
                        <tr>
                        <th>File name</th>
                        <th>Date modified</th>
                        <th>View</th>
                        <th>Modify</th>
                        <th>Delete</th>
                        </tr>
            '''                                              #build table header

            for file in dirs:
                if os.path.isfile(path+'/'+file):
                    time_modified = datetime.datetime.fromtimestamp(
                        os.path.getmtime(path+'/'+file)).strftime('%Y-%m-%d  %H:%M:%S')
                    row = '''<tr><td>{}</td><td>{}</td>
                            '''.format(file,time_modified)
                    files += row
            return files
        return '<h1>Server error! Your files have been corrupted!</h1>'
    else:
        return '<h1> Page is not available!</h1>'


def create_table_users():
    with sqlite3.connect('login.db') as connection:
        cursor = connection.cursor()
        cmd = '''
            CREATE TABLE IF NOT EXISTS users(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name char(40) NOT NULL,
            email char(50) NOT NULL,
            age INTEGER CHECK (age>0),
            username char(40) NOT NULL, 
            password char(200) NOT NULL)
            '''
        cursor.execute(cmd)


def insert_user(name,email,age,user,psw):
    with sqlite3.connect('login.db') as connection:
        cursor = connection.cursor()
        cmd = '''INSERT INTO users (name,email,age,username,password) VALUES(?,?,?,?,?)'''
        cursor.execute(cmd,(name,email,age,user,psw))
        connection.commit()


if __name__ == '__main__':
    create_table_users()
    #serve(app, listen='*:5000')
    app.run('localhost',5000)